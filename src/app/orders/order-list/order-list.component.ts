import { Component, OnInit } from '@angular/core';
import { OrdersModule } from '../orders.module';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  title ='Orders';
  constructor() { }

  ngOnInit() {
  }

}